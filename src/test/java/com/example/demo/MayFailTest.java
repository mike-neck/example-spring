package com.example.demo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MayFailTest {

    @Test
    public void failing() {
        assertEquals(1, 2);
    }

    @Test
    public void throwing() {
        throw new RuntimeException("foo");
    }

    @Test
    public void success() {
        assertEquals(1, 1);
    }
}
